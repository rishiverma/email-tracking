from django.db import models
from django.contrib.postgres.fields import JSONField
from django.db.models.signals import post_save
from django.dispatch import receiver


# Create your models here.


class TimeStampModel(models.Model):
    """
    Abstract model to store the create and update datetime.
    """
    _created_at = models.DateTimeField(auto_now_add=True, null=True)
    _updated_at = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        abstract = True


class Job(TimeStampModel):
    """
    Model to store Jobs.
    """
    STATES = (
        ('OPEN', 'OPEN'),
        ('CLOSED', 'CLOSED')
    )
    title = models.CharField(max_length=200)
    state = models.CharField(max_length=200, choices=STATES)
    opening_date = models.DateField()
    closing_date = models.DateField()


class Candidate(TimeStampModel):
    """
    Model to store candidates.
    """
    name = models.CharField(max_length=200)
    email = models.EmailField()
    date = models.DateField()


class Match(TimeStampModel):
    """
    Model includes the Match between job and candidate.
    """
    STATES = (
        ('IN_PROGRESS', 'IN_PROGRESS'),
        ('REPLIED', 'REPLIED'),
        ('COMPLETED', 'COMPLETED')

    )
    candidate = models.ForeignKey(Candidate, on_delete=models.CASCADE, related_name="candidate_match")
    job = models.ForeignKey(Job, on_delete=models.CASCADE, related_name="job_match")
    state = models.CharField(choices=STATES, max_length=100)
    candidate_interest = models.BooleanField()

    class Meta:
        unique_together = ('candidate', 'job')


class Event(TimeStampModel):
    """
    Model to store events received from 3rd party.~
    """
    date = models.DateTimeField()
    event_type = models.CharField(max_length=100)
    metadata = JSONField()
    match = models.ForeignKey("Match", on_delete=models.CASCADE, related_name="match_events", null=True)


class Email(TimeStampModel):
    """
    Model to store Email's generated from events.
    """
    Choices = (("CREATED", "CREATED"), ("BOUNCED", "BOUNCED"), ("RESPONDED", "RESPONDED"))
    date = models.DateTimeField()
    state = models.CharField(choices=Choices, default="CREATED", max_length=100)
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name="email_events")

    @receiver(post_save, sender=Event)
    def create_email(sender, instance, created, **kwargs):
        if created:
            if instance.event_type == "message.created":
                Email.objects.create(date=instance.date, state="CREATED", event=instance)
            if instance.event_type == "message.replied":
                Email.objects.create(date=instance.date, state="RESPONDED", event=instance)
            if instance.event_type == "message.bounced":
                Email.objects.create(date=instance.date, state="BOUNCED", event=instance)
