from django_rest_passwordreset.serializers import EmailSerializer
from rest_framework import serializers

from event_management.models import Job, Candidate, Match, Event


class JobSerializer(serializers.ModelSerializer):
    class Meta:
        model = Job
        fields = ('id', 'title', 'state', 'opening_date', 'closing_date')


class CandidateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Candidate
        fields = ("id", "name", "email", "date")


class MatchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Match
        fields = ('id', "candidate_interest", "state", "candidate", "job")

    def to_representation(self, instance):
        representation = super(MatchSerializer, self).to_representation(instance)
        representation['candidate'] = CandidateSerializer(instance.candidate).data
        representation['job'] = JobSerializer(instance.job).data
        return representation


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = ("id", "date", "event_type", "metadata", "match")


class EventListSerializer(serializers.Serializer):
    start_date = serializers.DateField(required=False)
    end_date = serializers.DateField(required=False)
    job_id = serializers.IntegerField(required=False)
    candidate_id = serializers.IntegerField(required=False)

    class Meta:
        fields = ("candidate_id", "job_id", "start_date", 'end_date')


class EventResponseSerializer(serializers.Serializer):
    eventBounced = serializers.IntegerField()
    eventResponded = serializers.IntegerField()
    eventCreated = serializers.IntegerField()
    allEvents = EventSerializer(many=True)

    class Meta:
        fields = ("eventBounced", "eventResponded", "eventCreated", "allEvents")
