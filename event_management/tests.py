from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status

from event_management.models import Candidate, Job
from datetime import timedelta, date


class EmailTrackerJobTestCases(TestCase):
    """
    This test class include test cases for job endpoint.
    """

    def test_valid_url(self):
        resp = self.client.get(reverse('create_list_job'))
        self.assertEqual(resp.status_code, 200)

    def test_for_create_jobs(self):
        self.client = Client()
        payload = {
            "id": "1",
            "title": "software developer",
            "state": "OPEN",
            "opening_date": "2020-03-01",
            "closing_date": "2021-03-03"
        }

        url = reverse('create_list_job')
        response = self.client.post(url, payload)
        expected_response = {'id': 1, 'title': 'software developer', 'state': 'OPEN', 'opening_date': '2020-03-01',
                             'closing_date': '2021-03-03'}
        assert response.data == expected_response
        assert response.status_code == status.HTTP_201_CREATED


class EmailTrackerCandidatesTestCases(TestCase):
    """
    This test class include test cases for candidate endpoint.
    """

    def test_valid_url(self):
        resp = self.client.get(reverse('create_list_candidate'))
        self.assertEqual(resp.status_code, 200)

    def test_for_create_candidate(self):
        self.client = Client()
        payload = {
            "name": "Sam",
            "email": "Sam@gmail.com",
            "date": "2020-01-03"
        }
        url = reverse('create_list_candidate')
        expected_response = {'id': 1, 'name': 'Sam', 'email': 'Sam@gmail.com', 'date': '2020-01-03'}

        response = self.client.post(url, payload)
        assert response.data == expected_response
        assert response.status_code == status.HTTP_201_CREATED


class EmailTrackerMatchesTestCases(TestCase):
    """
    This test class include test cases for match endpoint.
    """

    def setUp(self):
        self.candidate = Candidate.objects.create(name="test", email="test@gmail.com", date=date.today())

        self.job = Job.objects.create(title="Software Engineer", state="OPEN", opening_date=date.today(),
                                      closing_date=date.today() + timedelta(days=6))

    def test_valid_url(self):
        resp = self.client.get(reverse('create_list_match'))
        self.assertEqual(resp.status_code, 200)

    def test_for_create_Matches(self):
        payload = {
            "candidate": self.candidate.id,
            "job": self.job.id,
            "state": "COMPLETED",
            "candidate_interest": True
        }
        url = reverse('create_list_match')
        resp = self.client.post(url, payload)
        expected_response = {'id': 1, 'candidate_interest': True, 'state': 'COMPLETED',
                             'candidate': {'id': self.candidate.id, 'name': 'test', 'email': 'test@gmail.com',
                                           'date': '2022-05-31'},
                             'job': {'id': self.job.id, 'title': 'Software Engineer', 'state': 'OPEN',
                                     'opening_date': '2022-05-31', 'closing_date': '2022-06-06'}}

        assert resp.status_code == 201
        assert resp.data == expected_response
