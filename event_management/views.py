from datetime import datetime

from rest_framework import generics
from rest_framework.response import Response

from event_management.models import Job, Candidate, Match, Event
from event_management.serializers import JobSerializer, CandidateSerializer, MatchSerializer, EventSerializer, \
    EventListSerializer, EventResponseSerializer
from event_management.utils import filter_events


class JobsView(generics.ListCreateAPIView):
    """
    This class includes methods to create and list jobs.
    """
    serializer_class = JobSerializer
    queryset = Job.objects.all()


class CandidatesView(generics.ListCreateAPIView):
    """
    This class includes methods to create and list candidates.
    """
    serializer_class = CandidateSerializer
    queryset = Candidate.objects.all()


class MatchesView(generics.ListCreateAPIView):
    """
    This class includes methods to create and list match.
    """
    serializer_class = MatchSerializer
    queryset = Match.objects.all()


class EventsView(generics.ListCreateAPIView):
    """
    This class includes methods to create or list event.
    """
    serializer_class = EventSerializer
    queryset = Event.objects.all()

    def post(self, request):
        """
        This method is used to create event and email as per the given conditions.
        """
        try:
            data = request.data
            data["match"] = data.get('metadata').get('match_id')
            data["date"] = str(datetime.fromtimestamp(int(data.pop("date"))))
            serializer = EventSerializer(data=data)
            if serializer.is_valid(raise_exception=True):
                serializer.save()
                return Response(serializer.data, status=201)
        except ValueError as e:
            return Response({"error": str(e)}, status=500)

    def get(self, request):
        """
        this method is used to fetch the events.
        """
        serializer = EventListSerializer(data=request.GET)
        if serializer.is_valid(raise_exception=True):
            data = serializer.validated_data
            candidate_id = data.get('candidate_id')
            job_id = data.get('job_id')
            end_date = data.get("end_date")
            start_date = data.get("start_date")
            events = Event.objects.select_related()
            if candidate_id:
                events = events.filter(match__candidate__id=candidate_id)
            if job_id:
                events = events.filter(match__job__id=job_id)
            if start_date and end_date:
                events = events.filter(match__job__opening_date__lte=start_date, match__job__closing_date__gte=end_date)
            filtered_response = filter_events(events)
        return Response(EventResponseSerializer(filtered_response).data)
