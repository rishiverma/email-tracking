from django.db.models import Count, Q


def filter_events(events):
    """
    method to filter events from their event types..
    :param events:
    :return:
    """
    sorted_events = events.aggregate(
        bounced=Count('match', filter=Q(event_type='message.bounced')),
        created=Count('match', filter=Q(event_type='message.created')),
        replied=Count('match', filter=Q(event_type='message.replied')))
    return {"eventBounced": sorted_events["bounced"], "eventCreated": sorted_events["created"],
            "eventResponded": sorted_events["replied"],
            "allEvents": events}
