from django.urls import path

from event_management.views import JobsView, CandidatesView, MatchesView, EventsView

urlpatterns = [
    path('jobs/', JobsView.as_view(), name="create_list_job"),
    path('candidates/', CandidatesView.as_view(), name="create_list_candidate"),
    path('matches/', MatchesView.as_view(), name="create_list_match"),
    path('events/', EventsView.as_view(), name="create_list_event"),
]
