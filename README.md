
### Preparation

1. Create a virtual environment with pyenv or whatever you want
2. Activate the virtual environment
3. Execute the following command: pip install -r requirements.txt
4. Execute the migrations of django follow the command **python manage.py migrate**
5. Now, run the project: **python manage.py runserver**

### Run project in docker

- For the very first to initialize the project run -

```commandline
sudo docker-compose up --build
```

- Now to service will be available at 0.0.0.0:8000 Port we can run our endpoints directly

#### Endpoints

- To create job - http://localhost/jobs/
    - Method - POST
    - Payload:
    ```
     {"title":"Backend Engineer", "state":"OPEN", "opening_date":"2022-04-06", "closing_date":"2022-05-06"}
    ```
    - Response:
    ```
     {"id":26,"date":"1970-01-01T03:27:35Z","event_type":"message.created","metadata":{"message_id":"cjld2cjxh0000qzrmn831i7rn","match_id":4},"match":4}
    ```
  

- To create candidate - http://localhost/candidates/
  - Method - POST
  - Payload:
  ```
     name:TestName1
     email:test@email.com
     date:2020-02-05
  ```
  - Response: status 201
  ```
     {"id":2,"name":"TestName1","email":"test@email.com","date":"2020-02-05"}
  ```
  
- To create match between candidate and job - http://localhost/matches/
  - Method - POST
  - Payload:
  ```
     candidate:1
     job:8
     state:IN_PROGRESS
     candidate_interest:True
  ```
  - Response: status 201
  ```
     {"id":4,"candidate_interest":true,"state":"IN_PROGRESS","candidate":{"id":1,"name":"TestName1","email":"test@email.com","date":"2020-02-05"},"job":{"id":8,"title":"QA Engineer","state":"OPEN","opening_date":"2022-03-03","closing_date":"2022-04-04"}}
  ```
  
- To create event and mail - http://localhost/events/
    - Method: POST
    - Payload:
    ```
   { "date": "12455", "event_type": "message.created", "metadata": { "message_id": "cjld2cjxh0000qzrmn831i7rn", "match_id": 123 } }
    ```
    - Response: status 201
    ```
    {"id":1,"date":"1970-01-01T03:27:35Z","state":"CREATED","event":{"id":1,"date":"1970-01-01T03:27:35Z","event_type":"message.created","metadata":{"message_id":"cjld2cjxh0000qzrmn831i7rn","match_id":123}}}
    ```

- To fetch events on the basis of job and candidate - http://localhost/events/
  - Method - GET
  - Query Param: Optional
  ```
     job_id:1
     start_date:2022-03-07
     end_date:2022-04-01
     candidate_id:1
  ```
  - Response: status 200
  ```
    {"eventBounced":1,"eventResponded":1,"eventCreated":2,"allEvents":[{"id":21,"date":"1970-01-01T03:27:35Z","evnt_type":"message.bounced","metadata":{"match_id":4,"message_id":"cjld2cjxh0000qzrmn831i7rn"},"match":4},{"id":22,"date":"1970-01-01T03:27:35Z","event_type":"message.created","metadata":{"match_id":4,"message_id":"cjld2cjxh0000qzrmn831i7rn"},"match":4},{"id":23,"date":"1970-01-01T03:27:35Z","event_type":"message.replied","metadata":{"match_id":4,"message_id":"cjld2cjxh0000qzrmn831i7rn"},"match":4},{"id":24,"date":"1970-01-01T03:27:35Z","event_type":"message.hello","metadata":{"match_id":4,"message_id":"cjld2cjxh0000qzrmn831i7rn"},"match":4},{"id":25,"date":"1970-01-01T03:27:35Z","event_type":"message.created","metadata":{"match_id":4,"message_id":"cjld2cjxh0000qzrmn831i7rn"},"match":4}]}
  ```